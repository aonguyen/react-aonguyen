import { call, put, takeEvery } from "redux-saga/effects";
import FLIGHT from "contants/flight";
import { fetchFlight, searchFlight, addFlight, deleteFlight } from "apis/flightApi";
import {
    loadFlightSuccess,
    loadFlightError,
    searchFlightError,
    searchFlightSuccess,
    addFlightSuccess,
    addFlightError,
    deleteFlightSuccess,
    deleteFlightError
} from "../flightAction";

function* fetchFlightSaga() {
    try {
        const data = yield call(fetchFlight);
        yield put(loadFlightSuccess(data));
    } catch (error) {
        yield put(loadFlightError(error));
    }
}
function* searchFlightSaga({date}) {
    try {
        const data = yield call(searchFlight, date);
        yield put(searchFlightSuccess(data));
    } catch (error) {
        yield put(searchFlightError(error));
    }
}
function* addFlightSaga({flight}) {
    try {
        const data = yield call(addFlight, flight);
        yield put(addFlightSuccess(data));
    } catch (error) {
        yield put(addFlightError(error.response.data));
    }
}
function* deleteFlightSaga({flight}) {
    try {
        const data = yield call(deleteFlight, flight);
        yield put(deleteFlightSuccess(data));
    } catch (error) {
        yield put(deleteFlightError(error.response.data));
    }
}

export default function* watchFetchFlight() {
    yield takeEvery(FLIGHT.LOAD, fetchFlightSaga);
    yield takeEvery(FLIGHT.SEARCH, searchFlightSaga);
    yield takeEvery(FLIGHT.ADD, addFlightSaga);
    yield takeEvery(FLIGHT.DELETE, deleteFlightSaga);
}
