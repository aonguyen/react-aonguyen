import { flight } from '../contants';
import { message } from 'antd';
const {
    LOAD,
    LOAD_SUCCESS,
    LOAD_FAIL,

    SEARCH,
    SEARCH_SUCCESS,
    SEARCH_FAIL,

    ADD,
    ADD_SUCCESS,
    ADD_FAIL,

    DELETE,
    DELETE_SUCCESS,
    DELETE_FAIL
} = flight;

const loadFlight = () => ({
    type: LOAD,
});
const loadFlightSuccess = data => ({
    type: LOAD_SUCCESS,
    data
});
const loadFlightError = error => ({
    type: LOAD_FAIL,
    error
});
const searchFlights = date => ({
    type: SEARCH,
    date
});
const searchFlightSuccess = flights => ({
    type: SEARCH_SUCCESS,
    flights
});
const searchFlightError = error => ({
    type: SEARCH_FAIL,
    error
});
const addFlight = flight => ({
    type: ADD,
    flight
})
const addFlightSuccess = data => {
    message.success("Add flight success", 4);
    return {
        type: ADD_SUCCESS,
        data
    }
}
const addFlightError = error => {
    message.error(error, 4);
    return {
        type: ADD_FAIL,
    }
}
const deleteFlight = flight => ({
    type: DELETE,
    flight
})
const deleteFlightSuccess = flight => {
    message.success('Delete flight success', 4);
    return {
        type: DELETE_SUCCESS,
        flight
    }
}
const deleteFlightError = error => ({
    type: DELETE_FAIL,
    error
})

export {
    loadFlight,
    loadFlightSuccess,
    loadFlightError,
    searchFlights,
    searchFlightSuccess,
    searchFlightError,
    addFlight,
    addFlightSuccess,
    addFlightError,
    deleteFlight,
    deleteFlightSuccess,
    deleteFlightError
};