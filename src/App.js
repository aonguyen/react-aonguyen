import React from "react";
import { HashRouter } from "react-router-dom";
import { ConfigProvider, Empty } from "antd";
import { Provider } from "react-redux";
import RouterList from "./Routes/Routes";
import configStore from "./store";
import "./App.scss";

const store = configStore();

export default function App() {
    const renderEmpty = () => <Empty />;

    return (
        <Provider store={store}>
            <ConfigProvider renderEmpty={renderEmpty}>
                <HashRouter
                    // basename='/testHashRouter'
                    getUserConfirmation={(message, callback) => {
                        const allowTransation = window.confirm(message);
                        console.log(message);
                        callback(allowTransation);
                    }}
                >
                    <RouterList />
                </HashRouter>
            </ConfigProvider>
        </Provider>
    );
}
