import { combineReducers } from "redux";
import flightReducer from './flightReducer';

const rootReducer = combineReducers({
    flightReducer
});

export default rootReducer;
