import flight from "./../contants/flight";

const {
    LOAD,
    LOAD_FAIL,
    LOAD_SUCCESS,
    SEARCH,
    SEARCH_SUCCESS,
    SEARCH_FAIL,
    ADD_SUCCESS,
    DELETE_SUCCESS
} = flight;

const initialState = {
    flights: [],
    isLoading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case LOAD:
            return {
                ...state,
                isLoading: true,
            };
        case LOAD_SUCCESS:
            return {
                ...state,
                flights: action.data,
                isLoading: false,
            };
        case LOAD_FAIL:
            return {
                ...state,
                isLoading: false,
            };
        case SEARCH:
            return {
                ...state,
                isLoading: true,
            };
        case SEARCH_SUCCESS:
            return {
                ...state,
                flights: action.flights,
                isLoading: false,
            };
        case SEARCH_FAIL:
            return {
                ...state,
                isLoading: false,
            };
        case ADD_SUCCESS:
            return {
                ...state,
                flights: [{...action.data, $id: action.data.macb}, ...state.flights]
            }
        case DELETE_SUCCESS:
            const result = deleteFlight(state.flights, action.flight);
            return {
                ...state,
                flights: [...result]
            }
        default:
            return state;
    }
}

function deleteFlight(flights, flight) {
    const index = flights.findIndex(item => item.macb === flight.macb);
    if(index !== -1) {
        flights.splice(index, 1);
    }
    return flights;
}