import callAPI from "utils/callApi";

async function fetchFlight() {
    const response = await callAPI("GET", "/get-all", null);
    if (response.status >= 400) {
        throw new Error("Fetch fail");
    }
    return response.data;
}

async function searchFlight(date) {
    const response = await callAPI("GET", `/get-by-query?ngayDi=${date}`, null);
    if (response.status >= 400) {
        throw new Error("Search fail");
    }
    return response.data;
}

async function addFlight(flight) {
    const response = await callAPI("POST", "/insert", flight);
    return response.data;
}

async function deleteFlight(flight) {
    const response = await callAPI("PUT", '/delete', flight);
    return response.data;
}

export { fetchFlight, searchFlight, addFlight, deleteFlight };
