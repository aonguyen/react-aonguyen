import axios from "axios";

export default function callApi(method, endPoint, data) {
    return axios({
        method,
        url: `${process.env.REACT_APP_API_URL}${endPoint}`,
        data,
    });
}
