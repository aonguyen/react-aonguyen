import React, { Suspense } from "react";
import { Layout } from "antd";
import { GlobalHeader, GlobalFooter, SiderLeft, Logo, LazyLoad } from 'components';
import './styles.scss';

const { Header, Content, Footer, Sider } = Layout;

function BasicLayout({ children }) {
    return (
        <Layout>
            <Sider
                style={{
                    overflow: "auto",
                    height: "100vh",
                    position: "fixed",
                    left: 0,
                }}
            >
                <Logo />
                <SiderLeft />
            </Sider>
            <Layout className="site-layout" style={{ marginLeft: 200 }}>
                <Header
                    className="header"
                    style={{ padding: 0 }}
                >
                    <GlobalHeader />
                </Header>
                <Content style={{ margin: "24px 16px 0", overflow: "initial" }}>
                    <Suspense fallback={<LazyLoad />}>
                        <div className="content">
                            {children}
                        </div>
                    </Suspense>
                </Content>
                <Footer>
                    <GlobalFooter />
                </Footer>
            </Layout>
        </Layout>
    );
};

export default BasicLayout;
