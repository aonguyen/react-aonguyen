import React from "react";
import { Table, Tag } from 'antd';

export default function ExpandeFlight({ves}) {

    const columnsExpande = [
        { title: "Mã vé", dataIndex: "mave", key: "mave" },
        {
            title: "Thời gian đặt",
            dataIndex: "tgdat",
            key: "tgdat",
            render: (text, record) => !record.tgdat && 'Chưa có'
        },
        {
            title: "Số ghế",
            key: "soghe",
            dataIndex: "soghe",
        },
        {
            title: "Giá vé",
            dataIndex: "gia",
            key: "gia",
        },
        {
            title: "Trạng thái đặt",
            dataIndex: "dadat",
            key: "dadat",
            render: (status) =>
                status ? (
                    <Tag color="green">Đã đặt</Tag>
                ) : (
                    <Tag color="yellow">Chưa đặt</Tag>
                ),
        }
    ]

    return (
        <Table
            columns={columnsExpande}
            dataSource={ves}
            pagination={true}
            rowKey={(record) => record.$id}
            bordered
        />
    );
}
