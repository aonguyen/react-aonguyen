import React from "react";
import { Table, DatePicker, Space, Button, Popconfirm } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import moment from "moment";
import ExpandeFlight from "./../ExpandeFlight";

export default function FlightList({
    flights,
    isLoading,
    searchFlight,
    resetFlight,
    deleteFlight,
}) {
    const expandedRowRender = (flight) => <ExpandeFlight ves={flight.ves} />;

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        let date;
        if (Array.isArray(selectedKeys) && selectedKeys.length === 0) {
            date = moment(Date.now()).format("DD/MM/YYYY").toString();
            const formatDate = date.replace("/", "").replace("/", "");
            searchFlight(formatDate);
        } else {
            date = moment(selectedKeys).format("DD/MM/YYYY").toString();
            const formatDate = date.replace("/", "").replace("/", "");
            searchFlight(formatDate);
        }
    };
    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters,
        }) => (
            <div style={{ padding: 8 }}>
                <DatePicker
                    format="DD/MM/YYYY"
                    onChange={(e) => setSelectedKeys(e)}
                    value={selectedKeys !== "" ? moment(selectedKeys) : null}
                    style={{ width: 188, marginBottom: 8, display: "block" }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() =>
                            handleSearch(selectedKeys, confirm, dataIndex)
                        }
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
                    </Button>
                    <Button
                        onClick={() => resetFlight()}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Reset
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{ color: filtered ? "#1890ff" : undefined }}
            />
        ),
        render: (text) => moment(text).format("DD/MM/YYYY"),
    });

    const columns = [
        {
            title: "Mã chuyến bay",
            dataIndex: "macb",
            sorter: (a, b) => a.macb.length - b.macb.length,
        },
        {
            title: "Thời gian đi",
            dataIndex: "tgdi",
            render: (text, record) => moment(record.tgdi).format("DD/MM/YYYY"),
            ...getColumnSearchProps("tgdi"),
        },
        {
            title: "Thời gian đến",
            dataIndex: "tgden",
            render: (text, record) => moment(record.tgden).format("DD/MM/YYYY"),
        },
        {
            title: "Sân bay đi",
            dataIndex: "sbdi.tensb",
            render: (text, record) => record.sbdi.tensb,
        },
        {
            title: "Sân bay đến",
            dataIndex: "sbden.tensb",
            render: (text, record) => record.sbden.tensb,
        },
        {
            title: "Máy bay",
            dataIndex: "maybay.tenmb",
            render: (text, record) => record.maybay.tenmb,
        },
        {
            title: "Giá vé",
            dataIndex: "giave",
        },
        {
            title: "Hành động",
            render: (text, record) => (
                <Popconfirm
                    title="Are you sure delete this flight？"
                    okText="Yes"
                    cancelText="No"
                    onConfirm={() => deleteFlight(record)}
                    placement="topRight"
                >
                    <Button type="primary" danger>
                        Xóa
                    </Button>
                </Popconfirm>
            ),
        },
    ];

    return (
        <Table
            columns={columns}
            rowKey={(record) => record.$id}
            dataSource={flights}
            pagination={true}
            loading={isLoading}
            // onChange={handleTableChange}
            expandable={{ expandedRowRender }}
            bordered
        />
    );
}
