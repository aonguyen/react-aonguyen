import React from "react";
import { Form, Input, DatePicker } from "antd";

const { Item } = Form;

const formItemLayout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 12,
    },
};

export default function ContentAdd({form}) {

    return (
        <Form form={form} name="dynamic_rule">
            <Item
                {...formItemLayout}
                name="macb"
                label="Flight code"
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: "Please input flight code",
                    },
                ]}
            >
                <Input placeholder="Please input flight code" />
            </Item>
            <Item
                {...formItemLayout}
                name="masbdi"
                label="AirPort out code"
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: "Please input airport out code",
                    },
                ]}
            >
                <Input placeholder="Please input airport out code" />
            </Item>
            <Item
                {...formItemLayout}
                name="masbden"
                label="AirPort come code"
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: "Please input airport come code",
                    },
                ]}
            >
                <Input placeholder="Please input airport come code" />
            </Item>
            <Item
                {...formItemLayout}
                name="tgdi"
                label="Time go"
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: "Please input time go",
                    },
                ]}
            >
                <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" style={{width: '100%'}} />
            </Item>
            <Item
                {...formItemLayout}
                name="tgden"
                label="Time come"
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: "Please input time come",
                    },
                ]}
            >
                <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" style={{width: '100%'}} />
            </Item>
            <Item
                {...formItemLayout}
                name="mamb"
                label="Plane code"
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: "Please input plane code",
                    },
                ]}
            >
                <Input placeholder="Please input plane code" />
            </Item>
        </Form>
    );
}
