import React, { useState } from "react";
import { Button, Modal, Form } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import moment from 'moment';
import ContentAdd from "./../ContentAdd";
import './styles.scss';

export default function AddFlight({addFlight}) {
    const [visible, setVisible] = useState(false);
    const [form] = Form.useForm();

    function hideModal() {
        setVisible(false);
    }
    function showModal() {
        setVisible(true);
    }
    function handleOk(e) {
        form.validateFields().then((values) => {
            values.tgden = moment(values.tgden).toISOString();
            values.tgdi = moment(values.tgdi).toISOString();
            addFlight(values);
        }).then(() => {
            form.resetFields();
            setVisible(false);
        });
    }

    return (
        <div className='addWrapper'>
            <Button
                className='btn-add'
                type="primary"
                icon={<PlusOutlined />}
                onClick={showModal}
            >
                Add Flight
            </Button>
            <Modal
                // confirmLoading
                title="Add flight"
                visible={visible}
                onOk={handleOk}
                onCancel={hideModal}
                okText="Add Flight"
                cancelText="Cancel"
            >
                <ContentAdd form={form} />
            </Modal>
        </div>
    );
}
