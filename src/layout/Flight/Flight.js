import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col } from "antd";
import { loadFlight, searchFlights, addFlight, deleteFlight } from "actions/flightAction";
import FlightList from "./components/FlightList";
import AddFlight from "./components/AddFlight";

export default function Flight() {
    const flights = useSelector((state) => state.flightReducer.flights);
    const isLoading = useSelector((state) => state.flightReducer.isLoading);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(loadFlight());
    }, [dispatch]);

    function searchFlight(date) {
        dispatch(searchFlights(date));
    }
    function resetFlight() {
        dispatch(loadFlight());
    }
    function addNewFlight(flight) {
        dispatch(addFlight(flight));
    }
    function deleteOldFlight(record) {
        let oldFlight = {};
        
        oldFlight.macb = record.macb;
        oldFlight.masbdi = record.sbdi.masb;
        oldFlight.masbden = record.sbden.masb;
        oldFlight.tgdi = record.tgdi;
        oldFlight.tgden = record.tgden;
        oldFlight.mamb = record.maybay.mamb;
        
        dispatch(deleteFlight(oldFlight));
    }

    return (
        <Row>
            <Col span={24}>
                <AddFlight 
                    addFlight={addNewFlight}
                />
                <FlightList
                    flights={flights}
                    isLoading={isLoading}
                    searchFlight={searchFlight}
                    resetFlight={resetFlight}
                    deleteFlight={deleteOldFlight}
                />
            </Col>
        </Row>
    );
}
