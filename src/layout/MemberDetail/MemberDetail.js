import React, { Fragment } from "react";
import { Card, Row, Col, Anchor } from "antd";

const { Link } = Anchor;

export default function MemberDetail() {
    return (
        <Fragment>
            <Row gutter={[16, 16]}>
                <Col xxl={10} xl={9}>
                    <Card title="thong tin co ban"></Card>
                </Col>
                <Col xxl={10} xl={11}>
                    <Row gutter={[16, 16]}>
                        <Col span={24}>
                            <Card title="Hang the va diem"></Card>
                        </Col>
                    </Row>
                    <Row gutter={[16, 16]}>
                        <Col span={24}>
                            <Card title="Dai ly lien quan"></Card>
                        </Col>
                    </Row>
                    <Row gutter={[16, 16]}>
                        <Col span={24}>
                            <Card title="lien he"></Card>
                        </Col>
                    </Row>
                </Col>
                <Col xxl={4} xl={4}>
                    <Anchor>
                        <Link
                            href="#components-anchor-demo-basic"
                            title="Basic demo"
                        />
                        <Link
                            href="#components-anchor-demo-static"
                            title="Static demo"
                        />
                        <Link
                            href="#components-anchor-demo-basic"
                            title="Basic demo with Target"
                            target="_blank"
                        />
                        <Link href="#API" title="API">
                            <Link href="#Anchor-Props" title="Anchor Props" />
                            <Link href="#Link-Props" title="Link Props" />
                        </Link>
                    </Anchor>
                </Col>
            </Row>
            <Row gutter={[16, 16]}>
                <Col xxl={10} xl={9}><Card title="Truc tiep quan ly"></Card></Col>
                <Col xxl={10} xl={11}><Card title="Nghe nghiep va noi lam viec"></Card></Col>
            </Row>
            <Row gutter={[16, 16]}>
                <Col xxl={10} xl={9}><Card title="Chon ki doi thuong"></Card></Col>
                <Col xxl={10} xl={11}><Card title="Tai khoan ngan hang"></Card></Col>
            </Row>
            <Row gutter={[16, 16]}>
                <Col xxl={10} xl={9}><Card title="Quan ly hinh anh"></Card></Col>
                <Col xxl={10} xl={11}><Card title="So ghi chu"></Card></Col>
            </Row>
        </Fragment>
    );
}
