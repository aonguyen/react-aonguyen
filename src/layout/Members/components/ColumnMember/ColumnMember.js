import React from 'react';
import { Space, Tag, Tooltip } from 'antd';
import { EyeOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';

export default function ColumnMember(getColumnSearchProps) {

    const columns = [
        {
            title: 'Mã thành viên',
            dataIndex: 'code',
            key: 'code',
            align: 'center',
            render: (text, record) => <Link to='memberDetail'>{text}</Link>,
            ...getColumnSearchProps('code', 'code'),
        },
        {
            title: 'Tên thành viên',
            dataIndex: 'name',
            key: 'name',
            render: (text, record) => <Link to='memberDetail'>{text}</Link>,
            ...getColumnSearchProps('name', 'name'),
        },
        {
            title: 'Điểm tích lũy',
            dataIndex: 'point',
            key: 'point',
            align: 'center',
        },
        {
            title: 'Quyền lợi',
            key: 'quyenloi',
            dataIndex: 'quyenloi',
        },
        {
            title: 'Hạng thẻ',
            key: 'benefit',
            dataIndex: 'benefit',
        },
        {
            title: 'Nghề nghiệp',
            key: 'jobRole',
            dataIndex: 'jobRole',
            ...getColumnSearchProps('jobRole', 'jobRole'),
        },
        {
            title: 'CS',
            key: 'cs',
            dataIndex: 'cs',
        },
        {
            title: 'KDS',
            key: 'kds',
            dataIndex: 'kds',
        },
        {
            title: 'Đại lý',
            key: 'dealer',
            dataIndex: 'dealer',
            // ...getColumnSearchProps('dealer', 'dealer'),
        },
        {
            title: 'Nơi làm việc',
            key: 'working_location',
            dataIndex: 'working_location',
        },
        {
            title: 'Trạng thái',
            key: 'status',
            render: tags => {
                let color = 'geekblue';
                let status = 'tu choi kich hoat'
                if (tags.status === 1) {
                    color = 'purple';
                    status = 'Cho kich hoat';
                } else if (tags.status === 2) {
                    color = 'green';
                    status = 'hoat dong';
                } else if (tags.status === 3) {
                    color = 'blue';
                    status = 'Tam ngung';
                } else {
                    color = 'red';
                    status = 'Da khoa';
                }
                return <Tag color={color} style={{width: '100%', textAlign: 'center'}}>{status}</Tag>
            },
        },
        {
            title: 'Hệ máy',
            key: 'hemay',
            dataIndex: 'hemay',
            render: text => {
                let color = 'red';
                let hemay = 'Chua dang nhap'
                if(text === 1) {
                    hemay = 'IOS';
                    color = '#73d13d';
                } else if(text === 2) {
                    hemay = 'ANDROID';
                    color = '#13c2c2';
                }
                return <Tag color={color} style={{width: '100%', textAlign: 'center'}}>{hemay}</Tag>
            }
        },
        {
            title: 'Hành động',
            key: 'viewDetail',
            align: 'center',
            render: (text, record) => (
                <Space size="middle">
                    <Link to='/memberDetail'>
                        <Tooltip title='Xem chi tiết'>
                            <EyeOutlined />
                        </Tooltip>
                    </Link>
                </Space>
            ),
        },
    ];

    return (
        columns
    )
}