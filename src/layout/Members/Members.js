import React, { useState, useRef } from 'react';
import { Table, Input, Space, Button, Row, Col } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import data from './mock';
import { ColumnMember } from './components';
import './styles.scss';

export default function Members() {
    const [members, setMembers] = useState(data);
    let searchRef = useRef(null);

    const handleSearch = (selectedKeys, confirm, dataIndex, type) => {
        const memberList = [...members];
        let result = [];
        if (type === 'code') {
            result = memberList.filter(member => member.code.includes(selectedKeys[0]));
        } else if (type === 'name') {
            result = memberList.filter(member => member.name.toLowerCase().includes(selectedKeys[0].toLowerCase()));
        } else if(type === 'dealer') {
            result = memberList.filter(member => member.dealer.toLowerCase().includes(selectedKeys[0].toLowerCase()));
        }
        setMembers(result);
        confirm();
    };
    const handleReset = clearFilters => {
        clearFilters();
        setMembers(data);
    };

    const getColumnSearchProps = (dataIndex, type) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={searchRef}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex, type)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex, type)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
              </Button>
                    <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
              </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => searchRef.current.select());
            }
        },
        render: (text) => text
    });


    return (
        <Row>
            <Col span={24}>
                <Table columns={ColumnMember(getColumnSearchProps)} dataSource={members} bordered />
            </Col>
        </Row>
    )
}