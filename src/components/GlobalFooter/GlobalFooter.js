import React from 'react';
import './styles.scss';

export default function GlobalFooter() {
    return (
        <div className='footer'>
            <p>Ao Nguyen ©2020 Created by Ao Nguyen</p>
        </div>
    )
}
