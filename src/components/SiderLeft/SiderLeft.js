import React from "react";
import { Link, useLocation } from "react-router-dom";
import { Menu } from "antd";
import {
    TeamOutlined,
} from "@ant-design/icons";

export default function SiderLeft() {
    const location = useLocation();

    return (
        <Menu theme="dark" mode="inline" selectedKeys={[location.pathname]}>
            <Menu.Item key="/members" icon={<TeamOutlined />}  >
                <Link to="/members">Danh sách thành viên</Link>
            </Menu.Item>
        </Menu>
    );
}
