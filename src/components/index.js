export { default as GlobalHeader } from './GlobalHeader';
export { default as GlobalFooter } from './GlobalFooter';
export { default as SiderLeft } from './SiderLeft';
export { default as  Logo } from './Logo';
export { default as LazyLoad } from './LazyLoad';