import React, { lazy } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import BasicLayout from "layout/BasicLayout";
import NotFound from "layout/NotFound";

const Home = lazy(() => import("layout/Home"));
const Members = lazy(() => import('layout/Members'));
const MemberDetail = lazy(() => import('layout/MemberDetail'));

export default function Routes() {
    return (
        <Switch>
            <Redirect exact from="/" to="/home" />
            <Route
                path="/home"
                exact
                render={() => (
                    <BasicLayout>
                        <Home />
                    </BasicLayout>
                )}
            />
            <Route
                path="/members"
                exact
                render={() => (
                    <BasicLayout>
                        <Members />
                    </BasicLayout>
                )}
            />
            <Route
                path="/memberDetail"
                exact
                render={() => (
                    <BasicLayout>
                        <MemberDetail />
                    </BasicLayout>
                )}
            />
            <Route path="/notFound" exact render={() => <NotFound />} />
            <Redirect to="/notFound" />
        </Switch>
    );
}
